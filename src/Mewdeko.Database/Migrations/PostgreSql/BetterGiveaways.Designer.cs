﻿// <auto-generated />

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Mewdeko.Database.Migrations.PostgreSQL
{
    [DbContext(typeof(MewdekoPostgresContext))]
    [Migration("BetterGiveaways")]
    partial class BetterGiveaways
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            // required for reasons
        }
    }
}